﻿namespace NES;
public class VirtualMachine
{
    public readonly byte[] Cart;
    public ushort[] Memory;

    // Registers
    private byte _accumulator;
    private byte _x;
    private byte _y;
    private byte _stackPointer;
    private ushort _programCounter;
    private BitArray _statusRegister;

    public VirtualMachine(byte[] cart)
    {
        Cart = cart;
        Memory = new ushort[ushort.MaxValue];
        _accumulator = 0;
        _x = 0;
        _y = 0;
        _stackPointer = 1;
        _programCounter = 0;
        _statusRegister = new BitArray(8)
        {
            // 2 is always true because it's not actually used.
            [2] = true
        };
    }

    // Add two or one to the process counter and move on.
    public void Process()
    {
        var word = new Word(Word.CombineInstructions(Cart[_programCounter], Cart[_programCounter + 1]));
        switch (word.FourthNibble)
        {
            case 0x00:
                Console.WriteLine("No op");
                break;
            case 0x04:
                switch (word.ThirdNibble)
                {
                    case 0x04:
                        break;
                    case 0x0e:
                        Console.WriteLine("LSR");
                        _programCounter += 1;
                        _statusRegister.RightShift(1);
                        _statusRegister[2] = true;
                        break;
                }
                break;
        }
    }
}